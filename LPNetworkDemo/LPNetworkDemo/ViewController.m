//
//  ViewController.m
//  LPNetworkDemo
//
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import "ViewController.h"
#import "LPNetworkResponse.h"
#import "LPNetworkRequestConfiguration.h"
#import "LPNetworkRequest.h"
#import "LPNetworkResponseCache.h"
#import <CommonCrypto/CommonDigest.h>

static NSString *const kDemoTableViewCellIdentifier = @"kDemoTableViewCellIdentifier";

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ViewController {
    
    NSArray *_titles;
    UITableView *_demoTableView;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.navigationItem.title = @"Network Demo";
        _titles = @[@"GET请求",
                    @"POST请求",
                    @"批量发送请求",
                    @"请求缓存",
                    @"设置内置请求参数",
                    @"API签名实现",
                    @"block某些请求",
                    @"请求结果预处理",
                    @"发送多个串行请求",
                    @"图片上传",
                    @"操作队列中的请求",
                    @"取消指定URL的请求",
                    @"模拟登录token过期",
                    @"联网时加载动画",
                    @"统计联网时间",
                    @"增加全局请求头",
                    ];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [_demoTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadView {
    [super loadView];
    
    _demoTableView = [[UITableView alloc] init];
    _demoTableView.dataSource = self;
    _demoTableView.delegate = self;
    [_demoTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kDemoTableViewCellIdentifier];
    [self.view addSubview:_demoTableView];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    _demoTableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDemoTableViewCellIdentifier forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = _titles[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        [self invokeGetRequest];
    } else if (indexPath.row == 1) {
        [self invokePostRequest];
    } else if (indexPath.row == 2) {
        [self batchRequest];
    } else if (indexPath.row == 3) {
        [self cacheGetRequest];
    } else if (indexPath.row == 4) {
        [self globalParameters];
    } else if (indexPath.row == 5) {
        [self signAPIRequest];
    } else if (indexPath.row == 6) {
        [self blockRequest];
    } else if (indexPath.row == 7) {
        [self prehandleResponse];
    } else if (indexPath.row == 8) {
        [self sendChainRequests];
    } else if (indexPath.row == 9) {
        [self uploadLocalImage];
    } else if (indexPath.row == 10) {
        [self operatorRunningRequests];
    } else if (indexPath.row == 11) {
        [self cancelRequestByURL];
    } else if (indexPath.row == 12) {
        [self tokenExpired];
    } else if (indexPath.row == 13) {
        [self animationLoading];
    } else if (indexPath.row == 14) {
        [self calcRequestTime];
    } else if (indexPath.row == 15) {
        [self addHeaderParameters];
    }
}

#pragma mark - Request

- (void)invokeGetRequest {
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"one":@"two"} startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result %@", result.description);
    }];
}

- (void)invokePostRequest {
    [[LPNetworkRequest sharedInstance] POST:@"http://httpbin.org/post" parameters:@{@"one": @"two"} startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result: %@", result.description);
    }];
}

- (void)batchRequest {
    AFHTTPRequestOperation *request1 = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/delay/5" parameters:@{@"first":@"1"} startImmediately:NO configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"request1: %@", result.description);
    }];
    
    AFHTTPRequestOperation *request2 = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"second":@"2"} startImmediately:NO configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"request2: %@", result.description);
    }];
    
    [[LPNetworkRequest sharedInstance] batchOfRequestOperations:@[request1, request2] progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
        NSLog(@"请求完成 %ld/%ld", numberOfFinishedOperations, totalNumberOfOperations);
    } completionBlock:^() {
        NSLog(@"请求完成");
    }];
    
}

- (void)cacheGetRequest {
    AFHTTPRequestOperation *request1 = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"first":@"3"} startImmediately:NO configurationHandler:^(LPNetworkRequestConfiguration *configuration) {
        configuration.resultCacheDuration = 30;
    } completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"是否来自缓存1: %@", isFromCache ? @"是": @"否");
        NSLog(@"result1: %@", result.description);
    }];
    
    AFHTTPRequestOperation *request2 = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"first":@"3"} startImmediately:NO configurationHandler:^(LPNetworkRequestConfiguration *configuration) {
        configuration.resultCacheDuration = 30;
    } completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"是否来自缓存2: %@", isFromCache ? @"是": @"否");
        NSLog(@"result2: %@", result.description);
    }];
    
    [[LPNetworkRequest sharedInstance] addOperation:request1 toChain:@"cache"];
    [[LPNetworkRequest sharedInstance] addOperation:request2 toChain:@"cache"];
}

- (void)globalParameters {
    LPNetworkRequestConfiguration *configuration = [[LPNetworkRequestConfiguration alloc] init];
    configuration.builtinParameters = @{@"user": @"loopeer", @"token": @"7ca4a3cfe9b941b02452caf502907eb2"};
    [LPNetworkRequest sharedInstance].configuration = configuration;
    
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:nil startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        if (error) {
            NSLog(@"error: %@", error.description);
        } else {
            NSLog(@"result: %@", result.description);
        }
    }];
}

- (NSString *)md5String:(NSString *)string {
    const char *cStr = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (uint)strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

- (void)signAPIRequest {
    LPNetworkRequestConfiguration *configuration = [[LPNetworkRequestConfiguration alloc] init];
    configuration.builtinParameters = @{@"user": @"loopeer", @"token": @"7ca4a3cfe9b941b02452caf502907eb2"};
    [LPNetworkRequest sharedInstance].configuration = configuration;
    
    [LPNetworkRequest sharedInstance].parametersHandler = ^(NSMutableDictionary *builtinParameters, NSMutableDictionary *requestParameters) {
        NSString *builtinValues = [builtinParameters.allValues componentsJoinedByString:@""];
        NSString *requestValues = [requestParameters.allValues componentsJoinedByString:@""];
        
        NSString *md5Value = [self md5String:[NSString stringWithFormat:@"%@%@", builtinValues, requestValues]];
        requestParameters[@"sign"] = md5Value;
    };
    
    NSDictionary *requestParameters = @{@"page": @2, @"page_size": @30};
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:requestParameters startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result: %@", result.description);
    }];
}

- (void)blockRequest {
    LPNetworkRequestConfiguration *configuration = [[LPNetworkRequestConfiguration alloc] init];
    configuration.requestHandler = ^(AFHTTPRequestOperation *operation, id userInfo, BOOL *shouldStopProcessing){
        *shouldStopProcessing = YES;
    };
    [LPNetworkRequest sharedInstance].configuration = configuration;
    
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:nil startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        if (error.code == kLPNetworkResponseCancelError) {
            NSLog(@"request is block");
            NSLog(@"error: %@", error.description);
        }
    }];
}

- (void)prehandleResponse {
    LPNetworkRequestConfiguration *configuration = [[LPNetworkRequestConfiguration alloc] init];
    configuration.responseHandler = ^(AFHTTPRequestOperation *operation, id userInfo, LPNetworkResponse *response, BOOL *shouldStopProcessing) {
        NSDictionary *args = response.result[@"args"];
        if ([args[@"code"] integerValue] > 0) {
            response.error = [NSError errorWithDomain:args[@"message"] code:403 userInfo:nil];
            response.result = nil;
        }
    };
    
    [LPNetworkRequest sharedInstance].configuration = configuration;
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"code": @2, @"message": @"error message"} startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        if (error) {
            NSLog(@"error: %@", error.description);
        }
    }];
}

- (void)sendChainRequests {
    AFHTTPRequestOperation *request1 = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/delay/3" parameters:@{@"request":@1} startImmediately:NO configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result1: %@", result.description);
    }];
    
    AFHTTPRequestOperation *request2 = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"request":@2} startImmediately:NO configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result2: %@", result.description);
    }];
    
    AFHTTPRequestOperation *request3 = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"request":@2} startImmediately:NO configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result3: %@", result.description);
    }];
    
    AFHTTPRequestOperation *request4 = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"request":@2} startImmediately:NO configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result4: %@", result.description);
    }];
    
    [[LPNetworkRequest sharedInstance] addOperation:request1 toChain:@"cache"];
    [[LPNetworkRequest sharedInstance] addOperation:request2 toChain:@"cache"];
    [[LPNetworkRequest sharedInstance] addOperation:request3 toChain:@"cache"];
    [[LPNetworkRequest sharedInstance] addOperation:request4 toChain:@"cache"];
}


- (void)uploadLocalImage {
    UIImage *image = [UIImage imageNamed:@"upload.jpg"];
    
    NSDictionary *parameters = @{@"key": @"89DGILVW701a8754bcd0bff2ad687b3d301f45ec",
                                 @"format": @"json"};
    
    [[LPNetworkRequest sharedInstance] POST:@"https://post.imageshack.us/upload_api.php" parameters:parameters startImmediately:YES constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.75) name:@"fileupload" fileName:@"image.jpg" mimeType:@"image/jpeg"];
    } configurationHandler:nil completionHandler:^(NSError *error, id result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        if (!error) {
            NSLog(@"图片上传成功");
        } else {
            NSLog(@"error: %@", error.description);
        }
    }];
}

- (void)operatorRunningRequests {
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/delay/3" parameters:@{@"first": @1} startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"error: %@", error.description);
    }];
    
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:@{@"first": @2} startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"error: %@", error.description);
    }];
    
    NSLog(@"running requests: %@", [[LPNetworkRequest sharedInstance] runningRequests].description);
    [[LPNetworkRequest sharedInstance] cancelAllRequest];
}

- (void)cancelRequestByURL {
    NSString *urlString = @"http://httpbin.org/delay/9";
    [[LPNetworkRequest sharedInstance] GET:urlString parameters:@{@"first": @1} startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        if (error.code == kLPNetworkResponseCancelError) {
            NSLog(@"请求被取消");
        }
        NSLog(@"error: %@", error.description);
    }];
    
    [[LPNetworkRequest sharedInstance] cancelHTTPOperationsWithMethod:LPNetworkRequestMethodGet url:urlString];
}


- (void)tokenExpired {
    LPNetworkRequestConfiguration *configuration = [[LPNetworkRequestConfiguration alloc] init];
    configuration.responseHandler = ^(AFHTTPRequestOperation *operation, id userInfo, LPNetworkResponse *response, BOOL *shouldStopProcessing) {
        static BOOL hasExecuted;
        if (!hasExecuted) {
            hasExecuted = YES;
            *shouldStopProcessing = YES;
            
            static BOOL hasGotRefreshToken = NO;
            
            AFHTTPRequestOperation *refreshTokenRequestOperation = [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:nil startImmediately:NO configurationHandler:nil completionHandler:^(NSError *error, id result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
                if (!error) {
                    NSLog(@"got refresh token");
                } else {
                    NSLog(@"refresh token fetch failed:%@", error);
                }
                hasGotRefreshToken = YES;
            }];
            
            AFHTTPRequestOperation *reAssembledOperation = [[LPNetworkRequest sharedInstance] reAssembleOperation:operation];
            
            [[LPNetworkRequest sharedInstance] addOperation:refreshTokenRequestOperation toChain:@"refreshToken"];
            [[LPNetworkRequest sharedInstance] addOperation:reAssembledOperation toChain:@"refreshToken"];
        }
    };
    
    [LPNetworkRequest sharedInstance].configuration = configuration;
    
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/delay/2" parameters:nil startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result: %@", result.description);
    }];
}


- (void)animationLoading {
    LPNetworkRequestConfiguration *configuration = [[LPNetworkRequestConfiguration alloc] init];
    
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicatorView.frame = CGRectMake(self.view.frame.size.width / 2 - 8, self.view.frame.size.height / 2 - 8, 16, 16);
    [self.view addSubview:indicatorView];
    
    configuration.requestHandler = ^(AFHTTPRequestOperation *operation, id userInfo, BOOL *shouldStopProcessing) {
        if (userInfo[@"showLoading"]) {
            [indicatorView startAnimating];
        }
    };
    
    configuration.responseHandler = ^(AFHTTPRequestOperation *operation, id userInfo, LPNetworkResponse *response, BOOL *shouldStopProcessing) {
        if (userInfo[@"showLoading"]) {
            [indicatorView stopAnimating];
        }
    };
    
    [LPNetworkRequest sharedInstance].configuration = configuration;
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/delay/2" parameters:nil startImmediately:YES configurationHandler:^(LPNetworkRequestConfiguration *configuration) {
        configuration.userInfo = @{@"showLoading": @YES};
    } completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result: %@", result.description);
    }];
}


- (void)calcRequestTime {
    LPNetworkRequestConfiguration *configuration = [[LPNetworkRequestConfiguration alloc] init];
    NSTimeInterval startTime = [[NSDate date] timeIntervalSince1970];
    
    configuration.responseHandler = ^(AFHTTPRequestOperation *operation, id userInfo, LPNetworkResponse *response, BOOL *shouldStopProcessing) {
        NSLog(@"Request time:%f s", [[NSDate date] timeIntervalSince1970] - startTime);
    };
    
    [LPNetworkRequest sharedInstance].configuration = configuration;
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/delay/2" parameters:nil startImmediately:YES configurationHandler:^(LPNetworkRequestConfiguration *configuration){
        
    } completionHandler:^(NSError *error, id<NSObject> result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        NSLog(@"result: %@", result.description);
    }];
}


- (void)addHeaderParameters {
    LPNetworkRequestConfiguration *configuration = [LPNetworkRequest sharedInstance].configuration;
    configuration.builtinHeaders = @{@"version":@"1.2", @"platform":@"ios"};
    
    [[LPNetworkRequest sharedInstance] GET:@"http://httpbin.org/get" parameters:nil startImmediately:YES configurationHandler:nil completionHandler:^(NSError *error, id result, BOOL isFromCache, AFHTTPRequestOperation *operation) {
        if (!error) {
            NSLog(@"result: %@", [result description]);
        } else {
            NSLog(@"error: %@", error.description);
        }
    }];
}


@end