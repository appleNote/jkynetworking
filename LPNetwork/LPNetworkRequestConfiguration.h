//
//  LPNetworkRequestConfiguration.h
//  LoopeerNetworkDemo
//
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "LPNetworkResponse.h"

@interface LPNetworkRequestConfiguration : NSObject


@property (nonatomic, assign) NSInteger resultCacheDuration;

@property (nonatomic, copy) NSString *baseURL;

@property (nonatomic) AFHTTPRequestSerializer *requestSerializer;

@property (nonatomic) AFHTTPResponseSerializer *responseSerializer;

@property (nonatomic, copy) void (^responseHandler)(AFHTTPRequestOperation *operation, id userInfo, LPNetworkResponse *response, BOOL *shouldStopProcessing);

@property (nonatomic, copy) void (^requestHandler)(AFHTTPRequestOperation *operation, id userInfo, BOOL *shouldStopProcessing);

@property (nonatomic) id userInfo;

@property (nonatomic) NSDictionary *builtinParameters;

@property (nonatomic,strong) NSDictionary *builtinHeaders;



@end
