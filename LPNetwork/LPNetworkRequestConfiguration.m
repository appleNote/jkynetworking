//
//  LPNetworkRequestConfiguration.m
//  LoopeerNetworkDemo
//
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import "LPNetworkRequestConfiguration.h"

@implementation LPNetworkRequestConfiguration

- (AFHTTPRequestSerializer *)requestSerializer {
    return _requestSerializer ? : [AFHTTPRequestSerializer serializer];
}

- (AFHTTPResponseSerializer *)responseSerializer {
    return _responseSerializer ? : [AFJSONResponseSerializer serializer];
}

- (instancetype)copyWithZone:(NSZone *)zone {
    LPNetworkRequestConfiguration *configuration = [[LPNetworkRequestConfiguration alloc] init];
    configuration.baseURL = [self.baseURL copy];
    configuration.resultCacheDuration = self.resultCacheDuration;
    configuration.builtinParameters = [self.builtinParameters copy];
    configuration.userInfo = [self.userInfo copy];
    configuration.builtinHeaders = [self.builtinHeaders copy];
    return configuration;
}


@end
