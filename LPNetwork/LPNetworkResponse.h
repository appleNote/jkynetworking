//
//  LPNetworkResponse.h
//  LoopeerNetworkDemo
//
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const kLPNetworkResponseCancelError;

@interface LPNetworkResponse : NSObject


@property (nonatomic) NSError *error;
@property (nonatomic) id result;


@end
