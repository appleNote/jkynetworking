Pod::Spec.new do |s|

  s.name         = "jkyNetworking"
  s.version      = "0.0.6"
  s.summary      = "Common Network Library based on AFNetworking 2.0+"

  s.homepage     = "https://bitbucket.org/matrixeray/jkynetworking/overview"
  s.license      = "MIT"
  s.author       = { "matrixeray" => "june.key.young@gmail.com" }

  s.platform     = :ios, "7.0"

  s.source       = { :git => "https://matrixeray@bitbucket.org/matrixeray/jkynetworking.git", :tag => s.version }

  s.source_files  = "LPNetwork/*.{h,m}"

  s.dependency "AFNetworking", "2.5.3"
end
